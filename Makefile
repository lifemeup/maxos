bochs:
	bochs -f bochsrc.disk

boot:
	nasm mbr.asm -o mbr.bin

disk_write_boot:boot
	dd if=mbr.bin of=hd60m.img bs=512 count=1 conv=notrunc

loader:
	nasm loader.asm -o loader.bin

disk_write_loader:loader
	dd if=loader.bin of=hd60m.img bs=512 count=4 seek=2 conv=notrunc

compile_c:
	gcc -m32 -c -o kernel/main.o kernel/main.c

link_c:compile_c
	ld kernel/main.o -m elf_i386 -Ttext 0xc0001500 -e main -o kernel/kernel.bin

disk_write_kernel:compile_c link_c
	dd if=kernel/kernel.bin of=hd60m.img bs=512 count=200 seek=9 conv=notrunc

build_kernel:
	nasm -f elf -o build/print.o lib/kernel/print.S
	nasm -f elf -o build/kernel.o kernel/kernel.S

	gcc -m32 -I lib/kernel/ -I lib/ -I kernel/ -c -fno-builtin -o build/bitmap.o lib/kernel/bitmap.c
	gcc -m32 -I lib/kernel/ -I lib/ -I kernel/ -c -fno-builtin -o build/string.o lib/string.c
	gcc -m32 -I lib/kernel/ -I lib/ -I kernel/ -c -fno-builtin -o build/debug.o kernel/debug.c
	gcc -m32 -I lib/kernel/ -I lib/ -I kernel/ -c -fno-builtin -o build/timer.o device/timer.c
	gcc -m32 -I lib/kernel/ -I lib/ -I kernel/ -c -fno-builtin -o build/main.o kernel/main.c
	gcc -m32 -I lib/kernel/ -I lib/ -I kernel/ -c -fno-builtin -fno-stack-protector -o build/interrupt.o kernel/interrupt.c
	gcc -m32 -I lib/kernel/ -I lib/ -I kernel/ -c -fno-builtin -o build/init.o kernel/init.c

	ld -m elf_i386 -Ttext 0xc0001500 -e main -o kernel/kernel.bin build/main.o build/init.o build/interrupt.o build/print.o build/kernel.o build/timer.o build/debug.o build/string.o build/bitmap.o
	dd if=kernel/kernel.bin of=hd60m.img bs=512 count=200 seek=9 conv=notrunc

clean:
	rm -rf bochs.out mbr.bin loader.bin kernel/main.o kernel/kernel.bin kernel/tmp/test.bin kernel/print.o build/*