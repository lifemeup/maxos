[org 0x900]

%include "boot.inc"
section .text
start:
    ;magic_break
    xchg bx,bx
    ;open A20
    in al,0x92
    or al,00000010b
    out 0x92,al
    ;load gdt
    lgdt [gdt_ptr]
    ;set cr0 pe
    mov eax,cr0
    or eax,00000001b
    mov cr0,eax
    jmp dword SELECTOR_CODE:p_mode_start

[bits 32]
p_mode_start:
    ;magic_break
    xchg bx,bx
    mov ax, SELECTOR_DATA
    mov ds, ax
    mov es, ax
    mov ss, ax
    mov ax, SELECTOR_VIDEO
    mov gs, ax
    mov byte [gs:160],'P'
    jmp $

section .data
gdt_base:
    ;zeroth sector descriptor is zero and can't be indexed
    dd 0x00000000
    dd 0x00000000
    ;first sector descriptor
desc_code:
    dd  0x0000ffff
    dd  DESC_CODE_HIGH4
    ;second sector descriptor
desc_data:
    dd  0x0000ffff
    dd  DESC_DATA_HIGH4
    ;third sector descriptor
desc_video:
    dd  0x80000007
    dd  DESC_VIDEO_HIGH4

GDT_SIZE equ ($-gdt_base)
GDT_LIMIT equ (GDT_SIZE-1)
times 60 dq 0
SELECTOR_CODE equ (0x0001<<3)+TI_GDT+RPL_0
SELECTOR_DATA equ (0x0002<<3)+TI_GDT+RPL_0
SELECTOR_VIDEO equ (0x0003<<3)+TI_GDT+RPL_0

gdt_ptr:
    dw GDT_LIMIT
    dd gdt_base

loader_msg db "loader in real."